SndBuf mySound => dac;

// (1) Assembles file path/name and read file into SndBuf.
me.dir()+"samples/bass01.ogg" => mySound.read;

// (2) Asks the sound how long it is (in samples).
mySound.samples() => int numSamples;

// play sound once forward
0 => mySound.pos;
numSamples :: samp => now;  // (3) Lets it play for that long.
