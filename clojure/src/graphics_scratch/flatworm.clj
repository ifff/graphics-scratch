(ns graphics-scratch.core
  (:require [clojure2d.core :refer :all]
            [clojure2d.color :as c]
            [fastmath.core :as m]
            [fastmath.random :as r]))

;; for speed
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(def pal (c/random-palette))
(def num-pal-colors ^short (count pal))

(defn bumpy-circle [cnv fc]
  (doseq [^short q (range 400 0 -20)
          :let [s (for [^float a (range 0 (- m/TAU 0.1) 0.1)
                        :let [n (r/noise (+ 0.01 a) fc)
                              r (* q n)]]
                    [(* r (m/cos a)) (* r (m/sin a))])
                c (nth pal (mod (/ q 10) num-pal-colors))]]
    (filled-with-stroke cnv c :black path-bezier s true)))

(defn draw [cnv window ^long framecount state]
  (set-stroke cnv 0.1)
  (set-background cnv :black)
  (translate cnv 300 300)
  (bumpy-circle cnv (/ framecount 100.0)))

(def window (show-window (canvas 700 600) "Flatworm" draw))
