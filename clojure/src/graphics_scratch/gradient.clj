(ns graphics-scratch.gradient
  (:require [clojure2d.core :refer :all]
            [clojure2d.color :as c]
            [fastmath.core :as m]
            [fastmath.random :as r]
            [fastmath.vector :as v]))

;; for speed
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(def w 500)
(def hw (/ w 2))

(declare gradient)

(defn map-range [val min1 max1 min2 max2]
  (let [max1 (if (zero? max1)
               (inc max1)
               max1)]
    (- min2 (* max2 (/ (+ min1 val) max1)))))

(defn draw [cnv window ^long fc state]
  (set-background cnv :white)
  (doseq [x (range 0 w) y (range 0 w)
          :let [gradient-color (gradient x y)]]
    (set-color cnv gradient-color)
    (rect cnv x y 1 1)))

(defn gradient [x y]
  (let [r x
        g y
        b (map-range (m/sin (map-range (m/dist x y hw hw)
                                       0 (m/logb (+ x y) (+ hw (* 30 (m/sin (* 0.05 x)))
                                                                  (* 30 (m/cos (* 0.05 y))))) (- m/TAU) m/TAU)) -1 1 0 255)]
    (c/color r g b)))

(def window (show-window (canvas w w) "Gradient" draw))

