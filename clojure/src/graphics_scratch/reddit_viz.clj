(ns graphics-scratch.reddit-viz
  (:gen-class)
  (:require [clj-http.client :as http]
            [clojure.data.json :as json]
            [clojure2d.core :refer :all]
            [fastmath.core :as m]
            [fastmath.random :as r]))

;; (set! *warn-on-reflection* true)
;; (set! *unchecked-math* :warn-on-boxed)

(defrecord Post [title time])

(defn more-recent [a b]
  (> (:time a) (:time b)))

(def posts (atom (sorted-set-by more-recent)))

(defn parse-posts [resp]
  (let [resp-json (json/read-str resp)
        children (get-in resp-json ["data" "children"])]
    (map #(% "data") children)))

(defn process-response [resp]
  (let [raw-posts (parse-posts (:body resp))
        titles (map #(% "title") raw-posts)
        times (map #(% "created_utc") raw-posts)
        new-posts (set (map ->Post titles times))]
    (swap! posts (partial clojure.set/union new-posts))))

;; this scraper sorts by new
(defn get-posts [sub]
  (let [url (str "https://www.reddit.com/r/" sub "/new/.json?limit=1")]
    (try (process-response (http/get url))
         (catch clojure.lang.ExceptionInfo e
           (get-posts sub)))))

(defn vizualize [cnv window ^long fc state]
  (set-background cnv :black)
  (text cnv "Hello!" 100 100))

(defn -main
  "Runs the Reddit visualization"
  [& args]
  (doto (Thread. (fn [] (get-posts "me_irl")
                   (recur))) .start)
  (show-window (canvas 700 500) "Reddit Visualization" vizualize))
