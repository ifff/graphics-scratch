(ns graphics-scratch.noise
  (:require [clojure2d.core :refer :all]
            [clojure2d.color :as c]
            [fastmath.core :as m]
            [fastmath.random :as r]
            [fastmath.vector :as v]))

;; for speed
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(def w 500)
(def hw (/ w 2))

(def size 50)
(def hsize (/ size 2))

(defn mynoise
  ([x] (mynoise x 0))
  ([x y] (- hsize (* size (r/noise (+ 0.1 x) (+ 0.1 y))))))

(defn draw [cnv win ^long fc state]
  (set-color cnv (v/vec4 255 255 255 255))
  (translate cnv hw hw)
  (let [st (get-state win)
        [x1 y1] (:prev st)
        x2 (+ x1 (mynoise fc)) y2 (+ y1 (mynoise (* 0.1 fc)))]
    (set-state! win (assoc st :prev [x2 y2]))
    (line cnv x1 y1 x2 y2)))

(def window (show-window {:canvas (canvas w w)
                          :window-name "Noise"
                          :draw-fn draw
                          :state {:prev [0 0]}})
