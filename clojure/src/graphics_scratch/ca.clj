(ns graphics-scratch.ca
  (:require [clojure2d.core :refer :all]
            [clojure2d.color :as c]
            [fastmath.core :as m]
            [fastmath.random :as r]))

(defn cell [density]
  {:state (< (rand) density)
   :lifetime 0})

(defn init-cells [w h d]
  (for [x (range w)]
    (for [y (range h)]
      (cell d))))

(def ^:const grid-size 50)
(def ^:const cell-size 5)
(def cells (atom (init-cells grid-size grid-size 0.3)))

(defn show-cells [cnv cells]
  (doseq [x (range grid-size)
          y (range grid-size)
          :let [cell (nth (nth cells x) y)]]
    (set-color cnv (c/lerp :black :aqua (/ (:lifetime cell) 5.0)))
    (rect cnv (* cell-size x) (* cell-size y)
          cell-size cell-size)))

(defn nbs-with [f]
  (fn [cells x y]
    (reduce + (for [i (range -1 2) j (range -1 2)
                    :let [xi (mod (+ x i) grid-size)
                          yj (mod (+ y j) grid-size)
                          cell (nth (nth cells xi) yj)]
                    :when (not= i j 0)]
                (f cell [i j] [xi yj])))))

(defn sin [x]
  (case (mod x 4)
    0 1
    1 0
    2 -1
    3 0))

(defn cos [x]
  (sin (inc x)))

(def normal-nbs
  (nbs-with
   (fn [cell _ _]
     (if (:state cell) 1 0))))

(def linear-nbs
  (nbs-with
   (fn [cell [i j] [x y]]
     (if (and (:state cell)
              (not= (sin x) (cos y)))
     1 0))))

(defn update-cell [cells x y]
  (let [cell (nth (nth cells x) y)
        alive (:state cell)
        lifetime (:lifetime cell)
        nbs (normal-nbs cells x y)
        lin-nbs (linear-nbs cells x y)
        new-state #_(if alive
                    (or (= nbs 3) (= nbs 2))
                    (= nbs 3))
        (and (or (= lin-nbs 1) (= lin-nbs 2))
             (<= nbs 3))
        new-lifetime (if new-state (inc lifetime) 0)]
    {:state new-state :lifetime new-lifetime}))

;; TODO: see if you can use zippers for an extra challenge
(defn update-cells [cells]
  (for [x (range grid-size)]
    (for [y (range grid-size)]
      (update-cell cells x y))))

(defn draw [cnv window ^long framecount state]
  (show-cells cnv @cells)
  (swap! cells update-cells))

(def window (let [w (* grid-size cell-size)]
              (show-window (canvas w w) "Cellular Automaton" draw)))
