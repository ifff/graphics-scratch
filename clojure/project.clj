(defproject graphics-scratch "0.1.0-SNAPSHOT"
  :description "A `scratchpad' sort of thing where I just mess around with clojure2d."
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.json "0.2.6"]
                 [clj-http "3.9.1"]
                 [clojure2d "1.1.0"]
                 [generateme/fastmath "1.2.0"]]
  :repl-options {:init-ns graphics-scratch.core})
