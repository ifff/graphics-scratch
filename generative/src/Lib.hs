{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}

module Lib where

import Control.Monad.Random
import qualified Data.Vector as V
import Graphics.Rendering.Cairo
import Linear.V2
import Linear.V4
import qualified Numeric.Noise.Perlin as P

type Color = V4 Double
type Contour = V.Vector (V2 Double)

path :: Contour -> Render ()
path points = do
  newPath
  moveTo x1 y1
  mapM (\(V2 x y) -> lineTo x y) (V.tail points)
  closePath
    where (V2 x1 y1) = V.head points

draw :: IO ()
draw = do
  surf <- createImageSurface FormatARGB32 500 500
  renderWith surf sketch
  surfaceWriteToPNG surf "out.png"

setColor :: Color -> Render ()
setColor (V4 r g b a) = setSourceRGBA r g b a

rect :: Color -> V4 Double -> Render ()
rect color (V4 x y w h) = do
  setColor color
  rectangle x y w h
  fill

lavender :: Color
lavender = V4 0.5 0.2 0.3 1

radians :: Double -> Double
radians n = pi * (n / 180)

degrees :: Double -> Double
degrees n = 180 * (n / pi)

alphaMatte :: Render () -> Render () -> Render ()
alphaMatte matte src = do
  pushGroup
  src
  popGroupToSource
  pushGroup
  matte
  withGroupPattern mask

bg :: Render ()
bg = rect (V4 1 1 1 1) (V4 0 0 500 500)

grid :: Render ()
grid = do
  setColor (V4 0 0 0 1)
  path (V.fromList gridPoints)
  stroke
    where gridPoints = [10 * (V2 x y) | x <- [0..10], y <- [0..10]]

sketch :: Render ()
sketch = bg >> alphaMatte grid (rectangle 50 50 500 500)
